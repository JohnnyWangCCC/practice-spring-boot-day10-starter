package com.afs.restapi.service;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeResponseWithNoSalary;
import com.afs.restapi.dto.EmployeeUpdate;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<EmployeeResponseWithNoSalary> findAll() {
        return EmployeeMapper.toEmployeeResponseList(employeeRepository.findAllByStatusTrue());
    }

    public EmployeeResponseWithNoSalary update(int id, EmployeeUpdate toUpdate) {
        Employee employee = findById(id);
        if (toUpdate.getAge() != null) {
            employee.setAge(toUpdate.getAge());
        }
        if (toUpdate.getSalary() != null) {
            employee.setSalary(toUpdate.getSalary());
        }
        employeeRepository.save(employee);
        return EmployeeMapper.toEmployeeResponse(employee);
    }

    public Employee findById(int id) {
        return employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public EmployeeResponseWithNoSalary findByIdWithNoSalary(int id) {
        return employeeRepository.findByIdAndStatusTrue(id).map(EmployeeMapper::toEmployeeResponse)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public List<EmployeeResponseWithNoSalary> findByGender(String gender) {
        return EmployeeMapper.toEmployeeResponseList(employeeRepository.findByGenderAndStatusTrue(gender));
    }

    public List<EmployeeResponseWithNoSalary> findByPage(int pageNumber, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        return EmployeeMapper.toEmployeeResponseList(employeeRepository.findAllByStatusTrue(pageRequest).toList());
    }

    public EmployeeResponseWithNoSalary insert(EmployeeCreateRequest employeeCreateRequest) {
        return EmployeeMapper.toEmployeeResponse(employeeRepository.save(EmployeeMapper.toEntity(employeeCreateRequest)));
    }

    public void delete(int id) {
        Employee employee = findById(id);
        employee.setStatus(false);
        employeeRepository.save(employee);
    }
}
