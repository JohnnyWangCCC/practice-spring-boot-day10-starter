package com.afs.restapi.dto;

import com.afs.restapi.entity.Employee;

import javax.persistence.*;
import java.util.List;

public class CompanyResponse {
    private Integer id;
    private String name;

    private int employeesNumbers;

    public CompanyResponse() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEmployeesNumbers() {
        return employeesNumbers;
    }

    public void setEmployeesNumbers(int employeesNumbers) {
        this.employeesNumbers = employeesNumbers;
    }
}
