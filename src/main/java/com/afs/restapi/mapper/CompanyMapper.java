package com.afs.restapi.mapper;

import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class CompanyMapper {
    public static CompanyResponse toResponse(Company company){
        CompanyResponse companyResponse = new CompanyResponse();
        BeanUtils.copyProperties(company, companyResponse);
        companyResponse.setEmployeesNumbers(company.getEmployees().size());
        return companyResponse;
    }

    public static List<CompanyResponse> toResponseList(List<Company> companies){
        return companies.stream().map(CompanyMapper::toResponse).collect(Collectors.toList());
    }
}
