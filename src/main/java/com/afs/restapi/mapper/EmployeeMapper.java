package com.afs.restapi.mapper;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeResponseWithNoSalary;
import com.afs.restapi.dto.EmployeeUpdate;
import com.afs.restapi.entity.Employee;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class EmployeeMapper {
    public static Employee toEntity(EmployeeCreateRequest employeeCreateRequest){
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeCreateRequest, employee);
        return employee;
    }

    public static List<EmployeeResponseWithNoSalary> toEmployeeResponseList(List<Employee> employeeList) {
        return employeeList.stream().map(EmployeeMapper::toEmployeeResponse).collect(Collectors.toList());
    }

    public static EmployeeResponseWithNoSalary toEmployeeResponse(Employee employee) {
        EmployeeResponseWithNoSalary employeeResponseWithNoSalary = new EmployeeResponseWithNoSalary();
        BeanUtils.copyProperties(employee, employeeResponseWithNoSalary);
        return employeeResponseWithNoSalary;
    }

    public static EmployeeUpdate toEmployeeUpdate(Employee employee) {
        EmployeeUpdate employeeUpdate = new EmployeeUpdate();
        BeanUtils.copyProperties(employee, employeeUpdate);
        return employeeUpdate;
    }
}
