package com.afs.restapi.controller;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeResponseWithNoSalary;
import com.afs.restapi.dto.EmployeeUpdate;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("employees")
public class EmployeeController {

    private EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public List<EmployeeResponseWithNoSalary> getAllEmployees() {
        return employeeService.findAll();
    }

    @GetMapping("/{id}")
    public EmployeeResponseWithNoSalary getEmployeeById(@PathVariable int id) {
        return employeeService.findByIdWithNoSalary(id);
    }

    @GetMapping(params = {"gender"})
    public List<EmployeeResponseWithNoSalary> findEmployeesByGender(@RequestParam String gender) {
        return employeeService.findByGender(gender);
    }

    @GetMapping(params = {"page", "size"})
    public List<EmployeeResponseWithNoSalary> findByPage(int page, int size) {
        return employeeService.findByPage(page, size);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EmployeeResponseWithNoSalary insertEmployee(@RequestBody EmployeeCreateRequest employeeCreateRequest) {
        return employeeService.insert(employeeCreateRequest);
    }

    @PutMapping("/{id}")
    public EmployeeResponseWithNoSalary updateEmployee(@PathVariable int id, @RequestBody EmployeeUpdate employee) {
        return employeeService.update(id, employee);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable int id) {
        employeeService.delete(id);
    }
}
