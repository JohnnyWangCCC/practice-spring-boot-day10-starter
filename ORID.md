### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	Today's course learned about Mapper, constructing some DTOs to hide information shielded from the outside world, and using mapper to convert between DTOs or other entity mapping objects; and then introduced Flyway for version control of the database by introducing changes to the business; in the afternoon, we were exposed to Retro, and we also learned about the Retro Manifesto, and then had a small group Trainee Session on CI/CD, everyone performed well and increased their understanding of CI/CD through this.

### R (Reflective): Please use one word to express your feelings about today's class.

​	Bustling.

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	The most meaningful part of today's lesson was the debriefing of what we learned through group work, which reinforced our impressions while learning; Flyway was something I hadn't been exposed to in the past, and I was surprised by that.

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	Flyway has indeed been able to help with some projects in the past, and it's also a novelty to have a feature like this, which allows developers to be aware of changes to the business, and I try to incorporate what I've learned as much as I can into the business development.
